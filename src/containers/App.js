import React, { Component } from "react";
import MainStackRouter from "../Routers/MainStackRouter";
import { addNavigationHelpers } from "react-navigation";
import SidebarProvider from "./SidebarProvider";
class App extends Component {
  constructor(props) {
    console.log(props);
    super(props);
    this.state = {};
  }

  componentDidMount() {}
  render() {
    const newProps = { ...this.props };
     //screenProps={{ ...newProps }}
    delete newProps.dispatch;
    delete newProps.nav;
    const navigationHelper = addNavigationHelpers({
      dispatch: this.props.dispatch,
      state: this.props.nav
    });
    return (
      <SidebarProvider navigation={navigationHelper}>
        <MainStackRouter
          navigation={navigationHelper}
          screenProps={{}}
        />
      </SidebarProvider>
    );
  }
}

export default App;
