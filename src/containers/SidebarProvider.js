"use strict";

import React, { Component } from "react";
import { StyleSheet, View} from "react-native";

class SidebarProvider extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
            {this.props.children}
      </View>
    );
  }
}

const styles = StyleSheet.create({});

export default SidebarProvider;
