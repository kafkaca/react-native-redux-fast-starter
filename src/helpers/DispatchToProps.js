import { bindActionCreators } from 'redux'
import {StartActions} from '../actions'

export function navigate(action) {
	return (dispatch, getState) => {
		dispatch(navigateForward(action))
	}
}

export const StartDP = (dispatch) => {
	return bindActionCreators(StartActions, dispatch);
}

export const AppDP = (dispatch) => {
	return bindActionCreators(StartActions, dispatch);
}
