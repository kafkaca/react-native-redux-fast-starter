import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'

import {AppSP, StartSP} from './StateToProps'
import {StartDP,AppDP} from './DispatchToProps'

import App from '../containers/App'
import Start from '../components/Start'

export const NavigationState = connect(AppSP,AppDP)(App);
export const StartC = connect(StartSP,StartDP)(Start)
