import React, { Component } from 'react';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import {NavigationState} from './helpers/AllConnect'
  class Setup extends Component {
  render() {
    return (
      <Provider store={configureStore()}>
        <NavigationState />
      </Provider>
    );
  }
}

export default Setup;
