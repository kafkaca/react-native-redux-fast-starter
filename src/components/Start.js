"use strict";
import React, { Component, PropTypes } from "react";
import { StyleSheet, ScrollView } from "react-native";
import LoadinComponent from "./loading";
class Start extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return ( 
      <ScrollView style={styles.scrollView}>
      <LoadinComponent />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    flex: 1
  }
});

export default Start;
